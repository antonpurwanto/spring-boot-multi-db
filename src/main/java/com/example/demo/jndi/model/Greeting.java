package com.example.demo.jndi.model;

public class Greeting {
	
	private long counter;
	private String nama;
	
	public Greeting(long incrementAndGet, String format) {
		this.counter = incrementAndGet;
		this.nama = format;
	}
	
	public String sapa(String nama) {
		return nama;
	}

	public long getCounter() {
		return counter;
	}

	public void setCounter(long counter) {
		this.counter = counter;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}
	
}
