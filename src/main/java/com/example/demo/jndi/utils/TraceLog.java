package com.example.demo.jndi.utils;

import org.apache.log4j.Logger;

public class TraceLog {
	public static Logger getLogger(Class clazz) {
		return Logger.getLogger(clazz);
	}

	public static void trace(Class clazz, String message) {
		Logger.getLogger(clazz).trace(message);
	}

	public static void trace(Class clazz, String message, Throwable t) {
		Logger.getLogger(clazz).trace(message, t);
	}

	public static void trace(Class clazz, Throwable t) {
		trace(clazz, null, t);
	}

	public static void info(Class clazz, String message) {
		Logger.getLogger(clazz).info(message);
	}

	public static void info(Class clazz, String message, Throwable t) {
		Logger.getLogger(clazz).info(message, t);
	}

	public static void info(Class clazz, Throwable t) {
		info(clazz, null, t);
	}

	public static void debug(Class clazz, String message) {
		Logger.getLogger(clazz).debug(message);
	}

	public static void debug(Class clazz, Throwable t) {
		debug(clazz, null, t);
	}

	public static void debug(Class clazz, String message, Throwable t) {
		Logger.getLogger(clazz).debug(message, t);
	}

	public static void error(Class clazz, String message) {
		Logger.getLogger(clazz).error(message);
	}

	public static void error(Class clazz, Throwable t) {
		error(clazz, null, t);
	}

	public static void error(Class clazz, String message, Throwable t) {
		Logger.getLogger(clazz).error(message, t);
	}
}
