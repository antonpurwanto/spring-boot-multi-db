package com.example.demo.jndi.controller;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.jndi.konfig.Config;
import com.example.demo.jndi.model.Greeting;

@RestController
public class GreetingController {

    private static final String template = "Hello, %s! ";
    private final AtomicLong counter = new AtomicLong();

    @Autowired
    Config conf;

    @RequestMapping("/greeting")
    public Greeting greeting(@RequestParam(value = "name", defaultValue = "World") String name) {
        String ss=name;
        List<Map<String, Object>> a = new JdbcTemplate(conf.secondaryDs())
                .queryForList("SELECT * from latihan");
        for (Map<String, Object> map : a) {
            Set<String> kkeys = map.keySet();
            for (String string : kkeys) {
                if (map.get(string) != null)
                    ss += string + "  --  " + map.get(string).toString() + "## ";
            }
        }
        return new Greeting(counter.incrementAndGet(), String.format(template, ss));
    }

    @RequestMapping("/")
    public Greeting greetingBlank() throws Exception {
        String ss = null;
        List<Map<String, Object>> a = new JdbcTemplate(conf.primaryDs()).queryForList(
                "SELECT * from Orang");
        for (Map<String, Object> map : a) {
            Set<String> kkeys = map.keySet();
            for (String string : kkeys) {
                if (map.get(string) != null)
                    ss += string + "  --  " + map.get(string).toString() + "## ";
            }
        }
        return new Greeting(counter.incrementAndGet(), String.format(template, ss));
    }
}