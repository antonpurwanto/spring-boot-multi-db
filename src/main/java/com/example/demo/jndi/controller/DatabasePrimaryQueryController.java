package com.example.demo.jndi.controller;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.jndi.konfig.ConfigJdbcTemplates;
import com.example.demo.jndi.model.Greeting;

@RestController
public class DatabasePrimaryQueryController {

    private static final String template = "Hello, %s! ";
    private final AtomicLong counter = new AtomicLong();

    @Autowired
    ConfigJdbcTemplates confJts;

    @RequestMapping("/queryforpas")
    public Greeting greeting() throws Exception {
        String ss = null;
        List<Map<String, Object>> a = confJts.secondaryJT()
                .queryForList("SELECT * from latihan");
        for (Map<String, Object> map : a) {
            Set<String> kkeys = map.keySet();
            for (String string : kkeys) {
                if (map.get(string) != null)
                    ss += string + "  --  " + map.get(string).toString() + "## ";
            }
        }
        return new Greeting(counter.incrementAndGet(), String.format(template, ss));
    }

    @RequestMapping("/queryforods")
    public Greeting greetingBlank() throws Exception {
        String ss = null;
        List<Map<String, Object>> a = confJts.primaryJT().queryForList(
                "SELECT * from Orang");
        for (Map<String, Object> map : a) {
            Set<String> kkeys = map.keySet();
            for (String string : kkeys) {
                if (map.get(string) != null)
                    ss += string + "  --  " + map.get(string).toString() + "## ";
            }
        }
        return new Greeting(counter.incrementAndGet(), String.format(template, ss));
    }
}