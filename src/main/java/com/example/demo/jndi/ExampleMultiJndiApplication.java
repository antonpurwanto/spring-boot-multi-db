package com.example.demo.jndi;

import javax.sql.DataSource;

import org.apache.catalina.Context;
import org.apache.catalina.startup.Tomcat;
import org.apache.tomcat.util.descriptor.web.ContextResource;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainer;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;

import com.example.demo.jndi.utils.Foo;
import com.example.demo.jndi.utils.TraceLog;

@SpringBootApplication
public class ExampleMultiJndiApplication extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		TraceLog.info(this.getClass(), "Server startup data loading started");
		new Foo().bar();
		TraceLog.info(this.getClass(), "Server startup data loading completed");
		return super.configure(builder);
	}

	public static void main(String[] args) {
		SpringApplication.run(ExampleMultiJndiApplication.class, args);
	}

	@Bean
	public TomcatEmbeddedServletContainerFactory tomcatFactory() { // if datasouce defind in tomcat xml configuration
		// then no need to create this bean
		return new TomcatEmbeddedServletContainerFactory() {
			@Override
			protected TomcatEmbeddedServletContainer getTomcatEmbeddedServletContainer(Tomcat tomcat) {
				tomcat.enableNaming();
				return super.getTomcatEmbeddedServletContainer(tomcat);
			}

			@Override // create JNDI resource
			protected void postProcessContext(Context context) {

				ContextResource resource = new ContextResource();
				resource.setName("jndiDataSource");
				TraceLog.info(this.getClass(), resource.getName());
				resource.setType(DataSource.class.getName());
				resource.setProperty("factory", "org.apache.tomcat.jdbc.pool.DataSourceFactory");
				resource.setProperty("url", "jdbc:mysql://localhost:3306/test");
				resource.setProperty("username", "root");
				resource.setProperty("password", "java");
				context.getNamingResources().addResource(resource);

				ContextResource resource1 = new ContextResource();
				resource1.setName("jdbc/jndiDataSource");
				TraceLog.info(this.getClass(), resource1.getName());
				resource1.setType(DataSource.class.getName());
				resource1.setProperty("factory", "org.apache.tomcat.jdbc.pool.DataSourceFactory");
				resource1.setProperty("url", "jdbc:mysql://localhost:3306/coba?useSSL=false");
				resource1.setProperty("username", "root");
				resource1.setProperty("password", "java");
				context.getNamingResources().addResource(resource1);

			}
		};
	}
}
