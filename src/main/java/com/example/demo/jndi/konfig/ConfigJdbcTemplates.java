package com.example.demo.jndi.konfig;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.lookup.JndiDataSourceLookup;

@Configuration
public class ConfigJdbcTemplates {
    @Value("${foo.spring.datasource.jndi-name}")
    private String primaryJndiName;

    @Value("${bar.spring.datasource.jndi-name}")
    private String secondaryJndiName;

    @Primary
    @Bean(destroyMethod = "")

    public JdbcTemplate primaryJT() {
        JndiDataSourceLookup lookup = new JndiDataSourceLookup();
        return new JdbcTemplate(lookup.getDataSource(primaryJndiName));
    }

    @Bean(destroyMethod = "")

    public JdbcTemplate secondaryJT() {
        JndiDataSourceLookup lookup = new JndiDataSourceLookup();
        return new JdbcTemplate(lookup.getDataSource(secondaryJndiName));
    }
}

