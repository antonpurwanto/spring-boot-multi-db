package com.example.demo.jndi.konfig;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.datasource.lookup.JndiDataSourceLookup;

import com.example.demo.jndi.utils.TraceLog;

@Configuration
public class Config {
    @Value("${foo.spring.datasource.jndi-name}")
    private String primaryJndiName;

    @Value("${bar.spring.datasource.jndi-name}")
    private String secondaryJndiName;

    @Primary
    @Bean(destroyMethod = "")

    public DataSource primaryDs() {
        JndiDataSourceLookup lookup = new JndiDataSourceLookup();
        TraceLog.info(this.getClass(), primaryJndiName);
        return lookup.getDataSource(primaryJndiName);
    }

    @Bean(destroyMethod = "")

    public DataSource secondaryDs() {
        JndiDataSourceLookup lookup = new JndiDataSourceLookup();
        TraceLog.info(this.getClass(), secondaryJndiName);
        return lookup.getDataSource(secondaryJndiName);
    }
}
